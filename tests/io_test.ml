open OUnit
open Lib.Io


let sample_file_lines = [
    "Let us go then, you and I,";
    "When the evening is spread out against the sky";
    "Like a patient etherized upon a table;";
    "Let us go, through certain half-deserted streets,";
    "The muttering retreats";
    "Of restless nights in one-night cheap hotels";
    "And sawdust restaurants with oyster-shells:";
    "Streets that follow like a tedious argument";
    "Of insidious intent";
    "To lead you to an overwhelming question…";
    "Oh, do not ask, “What is it?”";
    "Let us go and make our visit.\n";
  ]


let slice list start take =
  let array = Array.of_list list in
  Array.sub array start take |> Array.to_list


let sample_file_contents = String.concat "\n" sample_file_lines


let make_test_file channel =
  Printf.fprintf channel "%s" sample_file_contents;
  flush channel


let test_read_file_lines =
  "test_read_file_lines" >::: [
      "target_line is 1-indexed" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let lines, _ = read_file_lines file 6 in
          assert_equal lines (slice sample_file_lines 5 1);
        in
        bracket_tmpfile test ()
      );
      "with before lines" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let lines, _ = read_file_lines ~before:3 file 6 in
          assert_equal lines (slice sample_file_lines 2 4)
        in
        bracket_tmpfile test ()
      );
      "with after lines" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let lines, _ = read_file_lines ~after:3 file 6 in
          assert_equal lines (slice sample_file_lines 5 4)
        in
        bracket_tmpfile test ()
      );
      "with before and after lines" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let lines, _ = read_file_lines ~before:3 ~after:3 file 6 in
          assert_equal lines (slice sample_file_lines 2 7)
        in
        bracket_tmpfile test ()
      );
      "when file doesn't exist" >:: (fun _ ->
        assert_raises (Sys_error "not-a-real-file: No such file or directory")
                      (fun () -> (read_file_lines "not-a-real-file" 12))
      );
      "too-large target line returns empty array" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let lines, _ = read_file_lines file 10000000 in
          assert_equal lines []
        in
        bracket_tmpfile test ()
      );
      "negative target line returns empty array" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let lines, _ = read_file_lines file (-3) in
          assert_equal lines []
        in
        bracket_tmpfile test ()
      );
      "target index points to right line for single line" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let _, index = read_file_lines ~before:0 ~after:0 file 1 in
          assert_equal index 0
        in
        bracket_tmpfile test ()
      );
      "target index points to right line with large before/after" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let _, index = read_file_lines ~before:20 ~after:12 file 5 in
          assert_equal index 4
        in
        bracket_tmpfile test ()
      );
    ]


let test_read_all_input =
  "test_read_all_input" >::: [
      "reads until end of file" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let read_channel = open_in file in
          let all_input = read_all_input read_channel in
          close_in read_channel;
          assert_equal all_input sample_file_contents
        in
        bracket_tmpfile test ()
      );
      "raises error if channel is closed" >:: (fun _ ->
        let test (file, channel) =
          make_test_file channel;
          let read_channel = open_in file in
          close_in read_channel;
          assert_raises (Sys_error "Bad file descriptor")
                        (fun () -> (read_all_input read_channel))
        in
        bracket_tmpfile test ()
      )
    ]


let tests =
  "Io" >::: [
      test_read_file_lines;
      test_read_all_input;
    ]
