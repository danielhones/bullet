open OUnit


let suite =
  "suite" >::: [
      Io_test.tests;
      Parser_test.tests;
      Source_code_test.tests;
      Traceback_test.tests;
    ]


let _ = run_test_tt_main suite
