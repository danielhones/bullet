open OUnit
open Lib.Traceback
open Lib.Parser


(* This traceback came from a Ruby 2.5.0 error *)
let ruby_tb =
  "Traceback (most recent call last):
        9: from /tmp/.rvm/rubies/ruby-2.5.0/bin/irb:11:in `<main>'
        8: from (irb):2
        7: from /tmp/ruby_requests/lib/requests/http_methods.rb:5:in `get'
        6: from /tmp/ruby_requests/lib/requests.rb:54:in `request'
        5: from /tmp/ruby_requests/lib/requests/session.rb:47:in `request'
        4: from /tmp/ruby_requests/lib/requests/session.rb:74:in `send_request'
        3: from /tmp/ruby_requests/lib/requests/adapters.rb:68:in `send_request'
        2: from /tmp/.rvm/rubies/ruby-2.5.0/lib/ruby/2.5.0/net/http.rb:910:in `start'
        1: from /tmp/ruby_requests/lib/requests/adapters.rb:70:in `block in send_request'
"

(* This traceback came from a Ruby 2.3.1 error *)
let other_ruby_tb =
  "/tmp/ruby-requests-0.0.1.a1/lib/requests/adapters.rb:69:in `block in send_request': I errored! (RuntimeError)
        from /tmp/.rvm/rubies/ruby-2.3.1/lib/ruby/2.3.0/net/http.rb:853:in `start'
        from /tmp/ruby-requests-0.0.1.a1/lib/requests/adapters.rb:68:in `send_request'
        from /tmp/ruby-requests-0.0.1.a1/lib/requests/session.rb:74:in `send_request'
        from /tmp/ruby-requests-0.0.1.a1/lib/requests/session.rb:47:in `request'
        from /tmp/ruby-requests-0.0.1.a1/lib/requests.rb:54:in `request'
        from /tmp/ruby-requests-0.0.1.a1/lib/requests/http_methods.rb:5:in `get'
        from /tmp/test.rb:5:in `<main>'
"


let test_ruby_parser =
  "test_ruby_parser" >::: [
      "first line ruby_tb" >:: (fun _ ->
        let tb = ruby_parser ruby_tb in
        let first = List.nth tb 0 in
        assert_equal first {
                       Line.file = "/tmp/.rvm/rubies/ruby-2.5.0/bin/irb";
                       Line.lineno = 11;
                       Line.label = Some "<main>"
                     }
      );
      "third line ruby_tb" >:: (fun _ ->
        let tb = ruby_parser ruby_tb in
        let first = List.nth tb 2 in
        assert_equal first {
                       Line.file = "/tmp/ruby_requests/lib/requests.rb";
                       Line.lineno = 54;
                       Line.label = Some "request"
                     }
      );
      "last line ruby_tb" >:: (fun _ ->
        let tb = ruby_parser ruby_tb in
        let last = List.nth tb (length tb - 1) in
        assert_equal last {
                       Line.file = "/tmp/ruby_requests/lib/requests/adapters.rb";
                       Line.lineno = 70;
                       Line.label = Some "block in send_request"
                     }
      );
      "first line other_ruby_tb" >:: (fun _ ->
        let tb = ruby_parser other_ruby_tb in
        let first = List.nth tb 0 in
        assert_equal first {
                       Line.file = "/tmp/ruby-requests-0.0.1.a1/lib/requests/adapters.rb";
                       Line.lineno = 69;
                       Line.label = Some "block in send_request"
                     }
      );
      "third line other_ruby_tb" >:: (fun _ ->
        let tb = ruby_parser other_ruby_tb in
        let first = List.nth tb 2 in
        assert_equal first {
                       Line.file = "/tmp/ruby-requests-0.0.1.a1/lib/requests/adapters.rb";
                       Line.lineno = 68;
                       Line.label = Some "send_request"
                     }
      );
      "last line other_ruby_tb" >:: (fun _ ->
        let tb = ruby_parser other_ruby_tb in
        let last = List.nth tb (length tb - 1) in
        assert_equal last {
                       Line.file = "/tmp/test.rb";
                       Line.lineno = 5;
                       Line.label = Some "<main>"
                     }
      );
    ]

let test_regex_parser =
  "test_regex_parser" >::: [
      "missing <label> group returns None in Traceback.t" >:: (fun _ ->
        let text = "    /tmp/foo.rb:78\n    /tmp/baz.rb:12\n" in
        let regex = "^ *(?P<file>.*):(?P<lineno>\\d+).*$" in
        let tb = regex_parser regex text in
        let expected = [
            {Line.file = "/tmp/foo.rb"; Line.lineno = 78; Line.label = None};
            {Line.file = "/tmp/baz.rb"; Line.lineno = 12; Line.label = None};
          ]
        in
        assert_equal tb expected
      )
    ]


let test_parse =
  "test_parse" >::: [
      "parses ruby" >:: (fun _ ->
        let tb = parse "ruby" ruby_tb in
        assert_equal (length tb) 8
      );
      "parses generic" >:: (fun _ ->
        let sample_tb = "/some/project/lib/file_a.foo:12
                         /some/project/lib/file_a.foo:37
                         /some/project/lib/file_b.foo:104
                         /some/shared/lib/file_c.foo:92"
        in
        let tb = parse "generic" sample_tb in
        assert_equal (length tb) 4
      );
      "raises Unsupported_language" >:: (fun _ ->
        assert_raises (Unsupported_language "\"foolang\" is not supported")
                      (fun () -> parse "foolang" "some traceback")
      )
    ]


let test_supported_languages =
  "test_supported_languages" >::: [
      "no supported languages cause error from for_lang" >:: (fun _ ->
        ignore @@ List.map for_lang supported_languages
      )
    ]


let tests =
  "Parser" >::: [
      test_ruby_parser;
      test_regex_parser;
      test_parse;
      test_supported_languages
    ]
