open OUnit
open Lib.Traceback


let test_to_string =
  "test_to_string" >::: [
      "single line" >:: (fun _ ->
        let tb = [
            { Line.file = "lib/requests.rb" ;
              Line.lineno = 54 ;
              Line.label = Some "request" }
          ]
        in
        let expected = "{file: \"lib/requests.rb\"; \
                        lineno: 54; label: \"request\"}" in
        assert_equal (to_string tb) expected
      );
      "several lines" >:: (fun _ ->
        let tb = [
            { Line.file = "lib/requests.rb" ;
              Line.lineno = 54 ;
              Line.label = Some "request" } ;
            { Line.file = "lib/requests/adapters.rb" ;
              Line.lineno = 69 ;
              Line.label = Some "block in send_request" } ;
            { Line.file = "lib/requests/adapters.rb";
              Line.lineno = 68;
              Line.label = Some "send_request" } ;
          ]
        in
        let expected = "\
{file: \"lib/requests.rb\"; lineno: 54; label: \"request\"}
{file: \"lib/requests/adapters.rb\"; lineno: 69; label: \"block in send_request\"}
{file: \"lib/requests/adapters.rb\"; lineno: 68; label: \"send_request\"}"
        in
        assert_equal (to_string tb) expected
      );
      "no lines" >:: (fun _ ->
        let tb = [] in
        assert_equal (to_string tb) ""
      );
      "no label" >:: (fun _ ->
        let tb = [
            { Line.file = "/tmp/test.rb" ;
              Line.lineno = 8 ;
              Line.label = None }
          ]
        in
        let expected = "{file: \"/tmp/test.rb\"; lineno: 8; label: \"\"}" in
        assert_equal (to_string tb) expected
      )
    ]

let tests =
  "Traceback" >::: [
    test_to_string
  ]
