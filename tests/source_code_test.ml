open OUnit
open Lib.Source_code
open Lib


let sample_code_lines = [
    "if ENV['REQUESTS_HTTP_DEBUG'] == 'true'" ;
    "    logger.debug(\"Setting Net::HTTP debug output to stdout\")" ;
    "    http.set_debug_output($stdout)" ;
    "end" ;
    "" ;
    "begin" ;
    "    resp = http.start do" ;
    "        http.request(request.raw)" ;
    "    end" ;
    "rescue Net::OpenTimeout => e" ;
    "    raise ConnectionTimeout.new('Connection timed out.'," ;
    "                                from_error: e, request: request)"
  ]


let slice list start take =
  let array = Array.of_list list in
  Array.sub array start take |> Array.to_list


let test_format_line =
  "test_format_line" >::: [
      "no code lines" >:: (fun _ ->
        let line = {
            Line.code_lines = [];
            Line.code_index = 0 ;
            Line.file = "lib/requests/adapters.rb" ;
            Line.lineno = 68 ;
          } in
        let expected = "lib/requests/adapters.rb:68\n\n" in
        assert_equal (format_line line) expected
      );
      "single code line" >:: (fun _ ->
        let line = {
            Line.code_lines = slice sample_code_lines 7 1;
            Line.code_index = 0 ;
            Line.file = "lib/requests/adapters.rb" ;
            Line.lineno = 68 ;
          } in
        let expected = 
          "lib/requests/adapters.rb:68\n->|        http.request(request.raw)\n"
        in
        assert_equal (format_line line) expected
      );
      "multiple code lines" >:: (fun _ ->
        let line = {
            Line.code_lines = slice sample_code_lines 2 8;
            Line.code_index = 5 ;
            Line.file = "lib/requests/adapters.rb" ;
            Line.lineno = 68 ;
          } in
        let expected = "\
lib/requests/adapters.rb:68
  |    http.set_debug_output($stdout)
  |end
  |
  |begin
  |    resp = http.start do
->|        http.request(request.raw)
  |    end
  |rescue Net::OpenTimeout => e
"
        in
        assert_equal (format_line line) expected
      )      
    ]


let make_sample_file (_, channel) =
  let sample_file_contents = String.concat "\n" sample_code_lines in
  Printf.fprintf channel "%s" sample_file_contents;
  flush channel


let test_read =
  "test_read" >::: [
      "empty traceback" >:: (fun _ ->
        assert_equal (read []) [];
      );
      "read single line" >:: (fun _ ->
        let test (file, channel) =
          make_sample_file (file, channel);
          let tb = [
              { Traceback.Line.file = file ;
                Traceback.Line.lineno = 3 ;
                Traceback.Line.label = None ;
              }
            ]
          in
          let expected = [
              { Line.code_lines = ["    http.set_debug_output($stdout)"] ;
                Line.code_index = 0 ;
                Line.file = file ;
                Line.lineno = 3 ;
              }]
          in
          assert_equal (read tb) expected;
        in
        bracket_tmpfile test ()
      );
      "read lines before" >:: (fun _ ->
        let test (file, channel) =
          make_sample_file (file, channel);
          let tb = [
              { Traceback.Line.file = file ;
                Traceback.Line.lineno = 8 ;
                Traceback.Line.label = None ;
              }
            ]
          in
          let lines = [
              "begin" ;
              "    resp = http.start do" ;
              "        http.request(request.raw)" ;
            ]
          in
          let expected = [
              { Line.code_lines = lines ;
                Line.code_index = 2 ;
                Line.file = file ;
                Line.lineno = 8 ;
              }]
          in
          assert_equal (read ~before:2 tb) expected
        in
        bracket_tmpfile test ()
      );
      "read lines after" >:: (fun _ ->
        let test (file, channel) =
          make_sample_file (file, channel);
          let tb = [
              { Traceback.Line.file = file ;
                Traceback.Line.lineno = 8 ;
                Traceback.Line.label = None ;
              }
            ]
          in
          let lines = [
              "        http.request(request.raw)" ;
              "    end" ;
              "rescue Net::OpenTimeout => e" ;
              "    raise ConnectionTimeout.new('Connection timed out.'," ;
            ]
          in
          let expected = [
              { Line.code_lines = lines ;
                Line.code_index = 0 ;
                Line.file = file ;
                Line.lineno = 8 ;
              }]
          in
          assert_equal (read ~after:3 tb) expected
        in
        bracket_tmpfile test ()
      );
      "read lines before and after" >:: (fun _ ->
        let test (file, channel) =
          make_sample_file (file, channel);
          let tb = [
              { Traceback.Line.file = file ;
                Traceback.Line.lineno = 8 ;
                Traceback.Line.label = None ;
              }
            ]
          in
          let lines = [
              "begin" ;
              "    resp = http.start do" ;
              "        http.request(request.raw)" ;
              "    end" ;
              "rescue Net::OpenTimeout => e" ;
              "    raise ConnectionTimeout.new('Connection timed out.'," ;
            ]
          in
          let expected = [
              { Line.code_lines = lines ;
                Line.code_index = 2 ;
                Line.file = file ;
                Line.lineno = 8 ;
              }]
          in
          assert_equal (read ~before:2 ~after:3  tb) expected;
        in
        bracket_tmpfile test ()
      );
      "read file that doesn't exist" >:: (fun _ ->
        let tb = [
              { Traceback.Line.file = "this/file/does-not/exist" ;
                Traceback.Line.lineno = 8 ;
                Traceback.Line.label = None ;
              }
          ]
        in
        let expected = [
            { Line.file = "this/file/does-not/exist" ;
              Line.code_lines = [] ;
              Line.code_index = -1 ;
              Line.lineno = 8 ;
            }
          ]
        in
        assert_equal (read tb) expected
      );
      "read multiple Traceback lines" >:: (fun _ ->
        let test (file, channel) =
          make_sample_file (file, channel);
          let tb = [
              { Traceback.Line.file = file ;
                Traceback.Line.lineno = 2 ;
                Traceback.Line.label = None
              } ;
              { Traceback.Line.file = file ;
                Traceback.Line.lineno = 8 ;
                Traceback.Line.label = None
              }
            ]
          in
          let expected = [
              { Line.file = file ;
                Line.code_lines = [
                    "if ENV['REQUESTS_HTTP_DEBUG'] == 'true'" ;
                    "    logger.debug(\"Setting Net::HTTP debug output to stdout\")" ;
                    "    http.set_debug_output($stdout)" ;
                  ] ;
                Line.code_index = 1 ;
                Line.lineno = 2;
              } ;
              { Line.file = file ;
                Line.code_lines = [
                    "    resp = http.start do" ;
                    "        http.request(request.raw)" ;
                    "    end" ;
                  ] ;
                Line.code_index = 1 ;
                Line.lineno = 8 ;
              }
            ]
          in
          assert_equal (read ~before:1 ~after:1 tb) expected
        in
        bracket_tmpfile test ()
      );
    ]


let tests =
  "Source_code" >::: [
      test_format_line;
      test_read;
    ]
