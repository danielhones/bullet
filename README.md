# Bullet

[![pipeline status](https://gitlab.com/danielhones/bullet/badges/master/pipeline.svg)](https://gitlab.com/danielhones/bullet/commits/master)
[![coverage report](https://gitlab.com/danielhones/bullet/badges/master/coverage.svg)](https://gitlab.com/danielhones/bullet/commits/master)

Bullet is a command-line tool for viewing source code from a traceback.  Given the lines of text from a traceback, Bullet finds the corresponding lines of source code and displays them on the screen.


## Installation

Use one of the links below to download a statically linked executable file that should run on any Linux machine:

* [Latest stable build][master build]
* [Latest develop build][develop build]

Download the file to a location in your `PATH` env variable and make it executable.  For example, assuming that `~/bin/` is a directory that exists in your `PATH`:

```
curl https://gitlab.com/danielhones/bullet/-/jobs/artifacts/develop/raw/bullet?job=build_executable > ~/bin/bullet && chmod +x ~/bin/bullet
```


## Usage

Bullet takes an error traceback over stdin and prints its output on stdout.

For example:

```
tail -n12 error_log.txt | bullet -l ruby -C 4 | less
```

Command line options (for a complete list, use `bullet --help`):

* `-r` reverses the order of the traceback
* `-B`, `-A`, `-C` work like they do with `grep`, taking an integer argument and printing the corresponding number of lines before or after the source line (or both for `-C`)
* `-l` specifies the language to use when parsing the traceback.  This defaults to "generic" which looks for the common format `filepath:lineno` in the traceback
* `--regex` takes a string as and argument that specifies a Perl-compatible regex to use when parsing the traceback.  This is useful if none of the options provided by `-l` are working.  The regex must have named groups called "file" and "lineno".  Named groups in the regex must use the PCRE format `(?P<name>...)`

If the output from a bullet command doesn't contain any of the source lines of code, this typically indicates that the parser isn't correctly identifying the filepath and line number.  To troubleshoot this, use the `--parse-only` option, which will print a representation of the filepath and line number being parsed.  Common problems include things like excess whitespace or extra words in the filepath.  The parse-only option is useful when building a custom regex for parsing tracebacks.


## Support and Contributing

* [Issue Tracker](https://gitlab.com/danielhones/bullet/issues)
* [Changelog](https://gitlab.com/danielhones/bullet/blob/master/CHANGELOG.md)
* [Contributing Guide](https://gitlab.com/danielhones/bullet/blob/develop/CONTRIBUTING.md)


## License

Bullet is licensed under the [MIT License](https://gitlab.com/danielhones/bullet/blob/master/LICENSE>)


[master build]: https://gitlab.com/danielhones/bullet/-/jobs/artifacts/master/raw/bullet?job=build_executable
[develop build]: https://gitlab.com/danielhones/bullet/-/jobs/artifacts/develop/raw/bullet?job=build_executable
