.PHONY : setup-coverage
setup-coverage :
	@sed -i.bak 's/^\( *(name lib) *\$\)/\1 (preprocess (pps bisect_ppx))/' lib/dune


.PHONY : coverage
coverage : setup-coverage
	@rm -f `find . -name 'bisect*.out'`
	@dune runtest --force
	@bisect-ppx-report -I _build/default/ -html _coverage/ \
		-text - `find . -name 'bisect*.out'`
	@mv lib/dune.bak lib/dune


.PHONY : static-exe
static-exe:
	dune clean
	cp bin/dune bin/dune.bak
	cp bin/.dunestatic bin/dune
	dune build bin/bullet.exe
	mv bin/dune.bak bin/dune
