# Changelog

This file tracks the notable changes to this project.  Its format is based on [Keep A Changelog](https://keepachangelog.com/en/1.0.0/).


## Unreleased

This section tracks the changes currently on [develop][develop] but not yet merged into [master][master] and tagged as a release.


## [1.0.0][1.0.0] - 2019-02-05 - [Download][1.0.0_download]

First release.

### Added

* Support for common Ruby tracebacks
* Support for generic tracebacks of the form "filepath:lineno"
* Option to reverse order of lines in the traceback
* Command line options to show lines within context of the source files
* `--regex` command line option to pass a regex to be used when parsing a traceback
* Rudimentary integration with Emacs and Vim


[develop]: https://gitlab.com/danielhones/bullet/tree/develop
[master]: https://gitlab.com/danielhones/bullet/tree/master
[1.0.0]: https://gitlab.com/danielhones/bullet/tree/v1.0.0
[1.0.0_download]: https://gitlab.com/danielhones/bullet/-/jobs/artifacts/v1.0.0/raw/bullet?job=build_executable
