FROM ocaml/opam:alpine_ocaml-4.06.0

RUN sudo mkdir /home/opam/code
ADD . /home/opam/code
WORKDIR /home/opam/code
RUN sudo chown -R opam:nogroup /home/opam/code

RUN opam switch 4.06.0 && \
    eval $(opam config env) && \
    opam repository add standard https://opam.ocaml.org/ && \
    opam update && \
    opam depext -y pcre && \
    opam install -y dune pcre ounit bisect_ppx
