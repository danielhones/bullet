open Lib


let version = "1.0.0"


let print_traceback_source ?(before=0) ?(after=0) tb =
  let f x = print_endline (Source_code.format_line x) in
  Source_code.read ~before ~after tb
  |> Source_code.iter f


let main () =
  let reverse = ref false in
  let before = ref 0 in
  let after = ref 0 in
  let lang = ref "generic" in
  let emacs = ref false in
  let vim = ref false in
  let parse_only = ref false in
  let regex = ref "" in
  let version_string =
    Printf.sprintf "Bullet v%s: Traceback viewing tool" version
  in
  let usage =
    Printf.sprintf "
    %s
    Pass a traceback over stdin

    For example:

        tail -n12 error_log.txt | bullet -B 6 -l ruby | less\n" version_string
  in
  let args = [
      ("-r", Arg.Set reverse,
       "Reverse the order of the lines in the traceback");
      ("--emacs", Arg.Set emacs,
       "Open the traceback files in Emacs.  There must be an Emacs \
        server running.");
      ("--vim", Arg.Set vim,
       "Open the traceback files in Vim");
      ("--parse-only", Arg.Set parse_only,
       "Only parse the traceback and print a representation of it");
      ("--regex", Arg.Set_string regex,
       "Use this regex to parse the traceback.  Must be a regex with  \
        named captured groups for 'file' and 'lineno'.  Named groups \
        must use the PCRE format (?P<name>...)");
      ("-l", Arg.Set_string lang,
       Printf.sprintf "Language to indicate how to parse traceback.  \
                       Must be one of: \"%s\".  Default is \"%s\""
                      (String.concat "\", \"" Parser.supported_languages)
                      !lang);
      ("-B", Arg.Set_int before,
       "Number of lines to show before traceback line.  Default is 0.");
      ("-A", Arg.Set_int after,
       "Number of lines to show after traceback line.  Default is 0.");
      ("-C", Arg.Int (fun x -> (before := x; after := x)),
       "Number of lines to show before and after traceback line. \
        Default is 0.");
      ("--version",
       Arg.Unit (fun _ -> (print_endline version_string; exit 0)), "")
    ] in
  Arg.parse args print_endline usage;
  let input = Io.read_all_input stdin in
  let parsed_input = if !lang = "regex"
                     then Parser.regex_parser !regex input
                     else Parser.parse !lang input
  in
  let traceback = if !reverse then
                    Traceback.rev parsed_input
                  else
                    parsed_input
  in
  if !parse_only then (
    (* This will probably be mainly used for debugging *)
    print_endline (Traceback.to_string traceback)
  )
  else if !emacs then
    Integrations.open_in_emacs traceback
  else if !vim then
    Integrations.open_in_vim traceback
  else
    print_traceback_source ~before:!before ~after:!after traceback


let () = main ()
