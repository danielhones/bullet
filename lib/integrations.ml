(*
  TODO: See how these handle opening the same file at different lines,
        which is common in a lot of tracebacks.  If possible, find a way
        to mark the multiple lines in a single file
 *)

let open_in_emacs tb =
  let f (tb_line : Traceback.Line.t) =
    Printf.sprintf "+%d %s" tb_line.lineno tb_line.file
  in
  let args = Traceback.map f tb in
  let cmd = Printf.sprintf "emacsclient %s"
                           (String.concat " " args) in
  (*
   TODO: It would be nice to also open a buffer with contents generated that
         will hyperlink to the other buffers we just opened
   *)
  ignore (Sys.command cmd)


(*
  TODO: Vim has a hardcoded limit of 10 as the maximum number of commands
        that can be passed on the command line. See if there's a better
        wayto handle the case where there are more than 10.
  TODO: Consider reversing this list before generating the Vim args because
        it seems to cycle through the buffers in the opposite order from
        the order they're passed on the command line
 *)
let open_in_vim tb =
  let f (tb_line : Traceback.Line.t) =
    Printf.sprintf "+\"edit +%d %s\"" tb_line.lineno tb_line.file
  in
  let args = Traceback.map f tb in
  let cmd = Printf.sprintf "vim - %s"
                           (String.concat " " args) in
  ignore (Sys.command cmd)
