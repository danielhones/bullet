let regex_parser regex text =
  let rex = Pcre.regexp ~flags:[`MULTILINE] regex in
  let matches = text |> Pcre.exec_all ~rex in
  let get_group = Pcre.get_named_substring rex in
  let build_from_match m = (
    {
      Traceback.Line.file = get_group "file" m;
      Traceback.Line.lineno = int_of_string (get_group "lineno" m);
      Traceback.Line.label = try Some (get_group "label" m)
                             with Invalid_argument _ -> None
    })
  in
  matches |> Array.map build_from_match |> Array.to_list


let ruby_parser =
  regex_parser "^(?:[ \\t]+from[ \\t]+|[ \\t]*\\d+: +from +)?\
                (?P<file>.*):(?P<lineno>\\d+):in `(?P<label>.*)'.*$"


let generic_parser =
  regex_parser "^[ \t]*(?P<file>.*):(?P<lineno>\\d+).*$"


exception Unsupported_language of string


(* 
TODO: Eventually look for regexes for languages first in a user config
      file, then these defaults, then error with Unsupported_language
      if nothing is found
*)
let for_lang lang =
  String.lowercase_ascii lang |>
    function
    | "ruby" -> ruby_parser
    | "generic" -> generic_parser
    | lang -> Unsupported_language (Printf.sprintf "%S is not supported" lang)
              |> raise;;


let supported_languages = ["generic"; "ruby"]


let parse lang text =
  text |> for_lang lang
