val regex_parser : string -> string -> Traceback.t

val ruby_parser : string -> Traceback.t

val generic_parser : string -> Traceback.t

exception Unsupported_language of string

val for_lang : string -> string -> Traceback.t

val parse : string -> string -> Traceback.t

val supported_languages : string list
