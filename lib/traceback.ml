module Line = struct
  type t =
    { file : string ;
      lineno : int ;
      label : string option ;
    }

  let to_string tb =
    let label_str = match tb.label with
      | Some x -> x
      | _ -> ""
    in
    Printf.sprintf "{file: %S; lineno: %d; label: %S}"
                   tb.file tb.lineno label_str
end


type t = Line.t list


let length = List.length
let iter = List.iter
let iteri = List.iteri
let map = List.map
let mapi = List.mapi
let rev = List.rev


let to_string tb = map Line.to_string tb |> String.concat "\n"
