module Line :
sig type t =
      { code_lines : string list ;
        code_index : int ;
        file : string ;
        lineno : int ;
      }
end

type t = Line.t list

val read : ?before:int -> ?after:int -> Traceback.t -> t

val length : t -> int

val iter : ('a -> unit) -> 'a list -> unit

val iteri : (int -> 'a -> unit) -> 'a list -> unit

val map : ('a -> 'b) -> 'a list -> 'b list

val mapi : (int -> 'a -> 'b) -> 'a list -> 'b list

val rev : 'a list -> 'a list

val format_line : Line.t -> string
