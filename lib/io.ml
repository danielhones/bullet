let read_all_input channel =
  let result = Buffer.create 1024 in
  let buffer = Bytes.create 1024 in
  let read_size = 1024 in
  let bytes_read = ref 1 in
  while !bytes_read > 0 do
    bytes_read := input channel buffer 0 read_size;
    Buffer.add_subbytes result buffer 0 !bytes_read;
  done;
  Buffer.contents result


let read_file_lines ?(before=0) ?(after=0) path target_line =
  let size = 1 + before + after in
  let first_line = target_line - before in
  let last_line = target_line + after in
  let lines = Array.make size "" in
  let channel = open_in path in
  let counter = ref 1 in
  let index = ref 0 in
  let target_index = ref 0 in
  while !counter <= last_line do
    try
      let this_line = input_line channel in
      if !counter >= first_line then (
        lines.(!index) <- this_line;
        if !counter = target_line then (
          target_index := !index
        );
        index := !index + 1
      ) else
        ();
      counter := !counter + 1
    with End_of_file ->
      counter := last_line + 1
  done;
  (Array.sub lines 0 !index |> Array.to_list,
   !target_index)
