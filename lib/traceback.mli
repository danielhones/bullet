module Line :
sig type t =
      { file : string;
        lineno : int;
        label : string option;
      }
end

type t = Line.t list

val length : t -> int

val iter : ('a -> unit) -> 'a list -> unit

val iteri : (int -> 'a -> unit) -> 'a list -> unit

val map : ('a -> 'b) -> 'a list -> 'b list

val mapi : (int -> 'a -> 'b) -> 'a list -> 'b list

val rev : 'a list -> 'a list

val to_string : t -> string
