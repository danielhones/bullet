module Line = struct
  type t =
  { code_lines : string list ;
    code_index : int ;
    file : string ;
    lineno : int ;
  }
end


type t = Line.t list


let length = List.length
let iter = List.iter
let iteri = List.iteri
let map = List.map
let mapi = List.mapi
let rev = List.rev


(*
 TODO: Consider using an Option for the code_index instead
       of -1 when the file does not exist
*)
let read ?(before=0) ?(after=0) traceback =
  traceback
  |> Traceback.map
       (fun (tbl : Traceback.Line.t) ->
         let code_lines, code_index =
           try Io.read_file_lines ~before ~after tbl.file tbl.lineno
           with Sys_error _ -> ([], -1)
         in
         { Line.code_lines = code_lines ;
           Line.code_index = code_index ;
           Line.file = tbl.file ;
           Line.lineno = tbl.lineno ;
       })


let format_line (src : Line.t) =
  let formatted_code =
    src.code_lines
    |> mapi (fun i line -> if i = src.code_index then
                             Printf.sprintf "->|%s" line
                           else
                             Printf.sprintf "  |%s" line)
  in
  let code = String.concat "\n" formatted_code in
  Printf.sprintf "%s:%d\n%s\n" src.file src.lineno code
