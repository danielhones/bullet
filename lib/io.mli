val read_all_input : in_channel -> string
(**
Read all the input from an in_channel and return it as a string.
For example, to read all the input from stdin:

let user_input = read_all_input stdin
*)

val read_file_lines : ?before:int -> ?after:int -> string -> int -> string list * int
(** 
Given a filepath and a line number to read from it, return a List of
lines from the file.  Here, target_line is the 1-indexed line number to read in
a file.

The optional before argument specifies how many lines before the target line
will also be returned, and the optional argument does the same for lines after
the target line
*)
