# Contributing to Bullet

Thanks for contributing to Bullet!  This document will contain steps and guidelines for setting up a development environment and contributing to the project.  In this repo, active development is done on the develop branch and is merged into master for stable releases.  Merge requests should be made with develop as the target branch.


## Development Environment

If you want to use Docker, there's a Dockerfile provided in this repo that builds a dev environemnt.  An image can also be downloaded from the Gitlab [container registry](https://gitlab.com/danielhones/bullet/container_registry) for this repo.  
It's also an option to create a local dev environment without Docker.  Setup should be fairly minimal and you can use the Dockerfile as a guide for installing the necessary dependencies:

```
opam switch 4.06.0
eval $(opam env)
opam depext -y pcre
opam install -y dune pcre ounit bisect_ppx
```

Tests can be run with `dune runtest`, or to include coverage, use `make coverage`.


## Opening a Merge Request

Open an issue to describe the feature you want to add or the bug you want to fix, if there's not already an open issue for it.  This is an important step to prevent wasted time for everybody.  If its agreed that a fix or implementation of the issue will be accepted, continue to the merge request steps:

1. Fork the repo
2. Run the tests with `dune runtest` on your system before making any changes.
3. To work on your changes, create a branch off of the develop branch.  Develop is the branch where all active development is done; master is for tracking releases and only contains stable, released code.
4. Make your changes and add/update tests to cover them.  If you add new test files, remember to add them to the suite defined in `tests/test.ml` or they won't actually be run.
5. Run the tests with ``make coverage`` and make sure all the tests pass and that coverage has not decreased.
6. Open a merge request from your feature/bugfix branch into develop.  Please include an explanation of what the branch fixes or what features it adds, and how to use it.

After the merge request is open, the CI pipeline will run.  Any failures in it will need to be addressed.  Once the pipeline passes, the code will be reviewed and changes might be requested.  After those changes are made, if any, and the pipeline passes again, the branch will be merged into develop.


## Code Style

There is no linter step yet in the CI/CD process, so just try to stick to a similar style as the existing code.  I like to keep line lengths less than 80 characters with few exceptions.
